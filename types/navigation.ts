import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

export type HomeStackParamList = {
  Home: undefined;
  PostDetail: {
    id: number;
  };
  Profile: {
    id: number;
  };
};

export type ExploreStackParamList = {
  Explore: undefined;
  PostDetail: {
    id: number;
  };
  Profile: {
    id: number;
  };
};

export type ShopStackParamList = {
  Shop: undefined;
};

export type ProfileStackParamList = {
  Profile: {
    id: number;
  };
  PostDetail: {
    id: number;
  };
};

// Home Screen
export type HomeScreenRouteProp = RouteProp<HomeStackParamList, 'Home'>;

export type HomeScreenNavigationProp = StackNavigationProp<
  HomeStackParamList,
  'Home'
>;

// Post Detail Screen
export type PostDetailScreenRouteProp = RouteProp<
  HomeStackParamList,
  'PostDetail'
>;

export type PostDetailScreenNavigationProp = StackNavigationProp<
  HomeStackParamList,
  'PostDetail'
>;

// Explore Screen
export type ExploreScreenRouteProp = RouteProp<
  ExploreStackParamList,
  'Explore'
>;

export type ExploreScreenNavigationProp = StackNavigationProp<
  ExploreStackParamList,
  'Explore'
>;

// Shop Screen
export type ShopScreenRouteProp = RouteProp<ShopStackParamList, 'Shop'>;

export type ShopScreenNavigationProp = StackNavigationProp<
  ShopStackParamList,
  'Shop'
>;

// Profile Screen
export type ProfileScreenRouteProp = RouteProp<
  ProfileStackParamList,
  'Profile'
>;

export type ProfileScreenNavigationProp = StackNavigationProp<
  ProfileStackParamList,
  'Profile'
>;
