// Basic Interface
export interface StrapiFile {
  name: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  width?: number;
  height?: number;
}

export interface StrapiImageFile extends StrapiFile {
  width: number;
  height: number;
}

export interface StrapiResponsiveImageFile extends StrapiImageFile {
  formats: {
    thumbnail: StrapiImageFile;
    small: StrapiImageFile;
    medium: StrapiImageFile;
  };
}

// Post Collection
export type Post = {
  id: number;
  caption: string;
  created_at: string;
  updated_at: string;
  image: StrapiResponsiveImageFile;
  users_permissions_user: UserPermissionUser;
};

// User-Permission User Collection
export type UserPermissionUser = {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  created_at: string;
  updated_at: string;
  posts: Post[];
};
