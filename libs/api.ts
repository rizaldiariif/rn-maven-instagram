import axios from 'axios';

export default axios.create({
  headers: {
    'app-id': '603e43135029972bd66e1164',
  },
});
