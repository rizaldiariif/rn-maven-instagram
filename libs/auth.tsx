import React, {useState, useEffect, useContext, createContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {UserPermissionUser} from '../types/api';
import api from './api';

const authContext = createContext<any>(null);

export function AuthProvider({children}: any) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};

export const checkLoggedInUser = async () => {
  const jwtToken = await AsyncStorage.getItem('jwt-strapi');

  let user = null;

  if (jwtToken) {
    const response = await axios.get(
      `https://strapi-dev.maven.co.id/users/me`,
      {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      },
    );

    user = response.data;
  }

  return user;
};

function useProvideAuth() {
  const [user, setUser] = useState<UserPermissionUser | null | false>(null);
  const [loading, setLoading] = useState<boolean>(true);

  const handleUser = async (user: UserPermissionUser | null | false) => {
    if (user) {
      setUser(user);
      setLoading(false);
      return user;
    } else {
      setUser(false);
      setLoading(false);
      return false;
    }
  };

  const signin = async ({
    identifier,
    password,
  }: {
    identifier: string;
    password: string;
  }) => {
    try {
      setLoading(true);
      const result = await api.post<any>(
        'https://strapi-dev.maven.co.id/auth/local',
        {identifier, password},
      );
      await AsyncStorage.setItem('jwt-strapi', result.data.jwt);
      handleUser(result.data.user);
    } catch (error) {
      console.log(error.response);
      setLoading(false);
      throw new Error('Error hehe');
    }
  };

  const signout = async () => {
    setLoading(true);
    // TODO create login logic
    await AsyncStorage.removeItem('jwt-strapi');
    handleUser(false);
  };

  const tempFunc = async () => {
    const logged_in_user = await checkLoggedInUser();
    handleUser(logged_in_user);
  };

  useEffect(() => {
    tempFunc();
  }, []);

  return {
    user,
    loading,
    signin,
    signout,
    handleUser,
  };
}
