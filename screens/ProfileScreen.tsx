import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import ProfileDescription from '../components/profile/ProfileDescription';
import ProfileHeader from '../components/profile/ProfileHeader';
import ProfileHighlight from '../components/profile/ProfileHighlight';
import ProfilePost from '../components/profile/ProfilePost';
import ProfileToolbar from '../components/profile/ProfileToolbar';
import posts from '../dev/post_data';
import api from '../libs/api';
import {UserPermissionUser} from '../types/api';
import {
  ProfileScreenNavigationProp,
  ProfileScreenRouteProp,
} from '../types/navigation';

type Props = {
  route: ProfileScreenRouteProp;
  navigation: ProfileScreenNavigationProp;
};

const ProfileScreen = (props: Props) => {
  const [user, setUser] = useState<UserPermissionUser | false>(false);

  const {route} = props;

  const {id} = route.params;

  const fetchUserById = async (id: number) => {
    const result = await api.get<UserPermissionUser>(
      `https://strapi-dev.maven.co.id/users/${id}`,
    );

    setUser(result.data);
  };

  useEffect(() => {
    fetchUserById(id);
  }, [id]);

  if (!user) {
    return (
      <View>
        <Text>Loading</Text>
      </View>
    );
  }

  return (
    <FlatList
      data={user.posts}
      keyExtractor={(item) => item.id.toString()}
      numColumns={3}
      showsVerticalScrollIndicator={false}
      renderItem={({item}) => (
        <ProfilePost post_id={item.id} image_url={item.image.url} />
      )}
      ListHeaderComponent={() => (
        <>
          <ProfileHeader post_count={user.posts.length} />
          <ProfileDescription username={user.username} />
          <ProfileToolbar />
          <ProfileHighlight />
        </>
      )}
    />
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({});
