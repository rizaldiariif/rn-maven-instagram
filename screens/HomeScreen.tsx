import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, FlatList, View} from 'react-native';
import api from '../libs/api';

import PostCaption from '../components/post/PostCaption';
import PostComment from '../components/post/PostComment';
import PostHeader from '../components/post/PostHeader';
import PostImage from '../components/post/PostImage';
import PostToolbar from '../components/post/PostToolbar';
import {
  HomeScreenNavigationProp,
  HomeScreenRouteProp,
} from '../types/navigation';
import {Post} from '../types/api';

// import posts from '../dev/post_data';

type Props = {
  route: HomeScreenRouteProp;
  navigation: HomeScreenNavigationProp;
};

const HomeScreen = (props: Props) => {
  const [posts, setPosts] = useState<Post[]>([]);

  const fetchPosts = async () => {
    const result = await api.get<Post[]>(
      'https://strapi-dev.maven.co.id/posts',
    );

    setPosts(result.data);
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
      <FlatList
        data={posts}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => (
          <View>
            <PostHeader
              username={item?.users_permissions_user?.username}
              user_id={item?.users_permissions_user?.id}
            />
            <PostImage id={item?.id} image_url={item?.image?.url} />
            <PostToolbar />
            <PostCaption
              username={item?.users_permissions_user?.username}
              user_id={item?.users_permissions_user?.id}
              caption={item?.caption}
            />
            <PostComment />
          </View>
        )}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({});

export default HomeScreen;
