import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';

import api from '../libs/api';

import PostCaption from '../components/post/PostCaption';
import PostComment from '../components/post/PostComment';
import PostHeader from '../components/post/PostHeader';
import PostImage from '../components/post/PostImage';
import PostToolbar from '../components/post/PostToolbar';
import {
  PostDetailScreenNavigationProp,
  PostDetailScreenRouteProp,
} from '../types/navigation';
import {Post} from '../types/api';

type Props = {
  route: PostDetailScreenRouteProp;
  navigation: PostDetailScreenNavigationProp;
};

const PostDetailHomeScreen = (props: Props) => {
  const [post, setPost] = useState<Post | false>(false);

  const {route} = props;

  const {id} = route.params;

  const fetchPostById = async (id: number) => {
    const result = await api.get(`https://strapi-dev.maven.co.id/posts/${id}`);

    setPost(result.data);
  };

  useEffect(() => {
    fetchPostById(id);
  }, [id]);

  if (!post) {
    return (
      <View>
        <Text>Loading</Text>
      </View>
    );
  }

  return (
    <View>
      <PostHeader
        username={post.users_permissions_user.username}
        user_id={post.users_permissions_user.id}
      />
      <PostImage id={post.id} image_url={post.image.url} />
      <PostToolbar />
      <PostCaption
        username={post.users_permissions_user.username}
        user_id={post.users_permissions_user.id}
        caption={post.caption}
      />
      <PostComment />
    </View>
  );
};

export default PostDetailHomeScreen;

const styles = StyleSheet.create({});
