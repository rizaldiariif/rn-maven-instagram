import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  Pressable,
  ImageBackground,
} from 'react-native';

// import posts from '../dev/post_data';
import api from '../libs/api';
import {Post} from '../types/api';
import {
  ExploreScreenNavigationProp,
  ExploreScreenRouteProp,
} from '../types/navigation';

type Props = {
  route: ExploreScreenRouteProp;
  navigation: ExploreScreenNavigationProp;
};

const ExploreScreen = (props: Props) => {
  const [posts, setPosts] = useState<Post[]>([]);

  const {navigation} = props;

  const fetchPosts = async () => {
    const result = await api.get<Post[]>(
      'https://strapi-dev.maven.co.id/posts',
    );

    setPosts(result.data);
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
      <FlatList
        data={posts}
        keyExtractor={(item) => item.id.toString()}
        numColumns={3}
        renderItem={({item}) => (
          <Pressable
            style={styles.post}
            onPress={() => navigation.navigate('PostDetail', {id: item.id})}>
            <ImageBackground
              source={{uri: `https://strapi-dev.maven.co.id${item.image.url}`}}
              style={styles.image_background}
            />
          </Pressable>
        )}
        columnWrapperStyle={styles.wrapper}
      />
    </SafeAreaView>
  );
};

export default ExploreScreen;

const styles = StyleSheet.create({
  wrapper: {
    flexGrow: 1,
  },
  post: {
    backgroundColor: 'transparent',
    height: 125,
    flexGrow: 1,
    borderWidth: 0.5,
    borderColor: 'white',
  },
  image_background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
