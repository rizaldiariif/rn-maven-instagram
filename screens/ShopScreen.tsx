import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';

interface Post {
  id: string;
  username: string;
}

const posts: Post[] = [
  {
    id: '1',
    username: 'user_1',
  },
  {
    id: '2',
    username: 'user_2',
  },
  {
    id: '3',
    username: 'user_3',
  },
  {
    id: '4',
    username: 'user_4',
  },
  {
    id: '5',
    username: 'user_5',
  },
  {
    id: '6',
    username: 'user_6',
  },
  {
    id: '7',
    username: 'user_7',
  },
];

const ShopScreen = () => {
  return (
    <SafeAreaView>
      <FlatList
        data={posts}
        keyExtractor={(item) => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => (
          <View style={styles.header_item}>
            <Text>{item.username}</Text>
          </View>
        )}
      />
      <FlatList
        data={posts}
        keyExtractor={(item) => item.id}
        numColumns={2}
        style={styles.body}
        renderItem={({item}) => (
          <View style={styles.body_item_container}>
            <View style={styles.body_item}>
              <Text>{item.username}</Text>
            </View>
            <Text>{item.username}</Text>
          </View>
        )}
      />
    </SafeAreaView>
  );
};

export default ShopScreen;

const styles = StyleSheet.create({
  header_item: {
    paddingHorizontal: 40,
    paddingVertical: 20,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: 'darkgrey',
    marginHorizontal: 10,
    marginVertical: 10,
    backgroundColor: 'white',
  },
  body: {
    paddingBottom: 50,
  },
  body_item_container: {
    flexGrow: 1,
    padding: 20,
  },
  body_item: {
    flexGrow: 1,
    height: 150,
    padding: 20,
    borderRadius: 15,
    backgroundColor: 'gray',
  },
});
