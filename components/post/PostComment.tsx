import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const PostComment = () => {
  return (
    <View style={styles.container}>
      <Text>View 10 Comment</Text>
    </View>
  );
};

export default PostComment;

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
});
