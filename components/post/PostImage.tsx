import React from 'react';
import {ImageBackground, Pressable, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

interface Props {
  id: number;
  image_url: string;
}

const PostImage = (props: Props) => {
  const {id, image_url} = props;

  const navigation = useNavigation();

  return (
    <Pressable
      style={styles.image}
      onPress={() => navigation.navigate('PostDetail', {id: id})}>
      <ImageBackground
        source={{uri: `https://strapi-dev.maven.co.id${image_url}`}}
        style={styles.backgroundImage}
      />
    </Pressable>
  );
};

export default PostImage;

const styles = StyleSheet.create({
  image: {
    flexGrow: 1,
    height: 300,
    backgroundColor: 'transparent',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
