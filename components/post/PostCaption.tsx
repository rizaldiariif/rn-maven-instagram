import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

interface Props {
  username: string;
  caption: string;
  user_id: number;
}

const PostCaption = (props: Props) => {
  const [viewCaption, setViewCaption] = useState(false);

  const {username, caption, user_id} = props;

  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text numberOfLines={viewCaption ? 99 : 2}>
        <Text
          style={styles.username}
          onPress={() => navigation.navigate('Profile', {id: user_id})}>
          {username}{' '}
        </Text>
        <Text>{caption}</Text>
      </Text>
      {!viewCaption && (
        <Text onPress={setViewCaption.bind(this, true)}>More</Text>
      )}
    </View>
  );
};

export default PostCaption;

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  username: {
    fontWeight: 'bold',
  },
});
