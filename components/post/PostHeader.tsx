import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

interface Props {
  username: string;
  user_id: number;
}

const PostHeader = (props: Props) => {
  const {username, user_id} = props;

  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <View style={styles.header_left}>
        <View style={styles.avatar} />
        <Text
          style={styles.username}
          onPress={() => navigation.navigate('Profile', {id: user_id})}>
          {username}
        </Text>
      </View>
      <Text>O</Text>
    </View>
  );
};

export default PostHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  header_left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 30,
    backgroundColor: 'gray',
  },
  username: {
    marginLeft: 10,
    fontSize: 14,
    fontWeight: 'bold',
  },
});
