import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const PostToolbar = () => {
  return (
    <View style={styles.container}>
      <View style={styles.toolbar_left}>
        <Text>X</Text>
        <Text>X</Text>
        <Text>X</Text>
      </View>
      <Text>X</Text>
    </View>
  );
};

export default PostToolbar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  toolbar_left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
