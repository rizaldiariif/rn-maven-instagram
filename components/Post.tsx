import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

interface IPostProps {
  id: string;
  username: string;
  handlePress: () => void;
}

const Post = (props: IPostProps) => {
  const {username, handlePress} = props;

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.header_left}>
          <View style={styles.avatar} />
          <Text style={styles.username}>{username}</Text>
        </View>
        <Text>O</Text>
      </View>
      <TouchableOpacity style={styles.body} onPress={handlePress} />
      <View style={styles.toolbar}>
        <View style={styles.toolbar_left}>
          <Text>X</Text>
          <Text>X</Text>
          <Text>X</Text>
        </View>
        <Text>X</Text>
      </View>
    </View>
  );
};

export default Post;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  header_left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 30,
    backgroundColor: 'gray',
  },
  username: {
    marginLeft: 10,
    fontSize: 14,
    fontWeight: 'bold',
  },
  body: {
    flexGrow: 1,
    height: 300,
    backgroundColor: 'gray',
  },
  toolbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  toolbar_left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
