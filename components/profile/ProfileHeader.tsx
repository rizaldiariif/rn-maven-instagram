import React from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import {useAuth} from '../../libs/auth';

type Props = {
  post_count: number;
};

const ProfileHeader = (props: Props) => {
  const {post_count} = props;

  const auth = useAuth();

  return (
    <View style={styles.container}>
      <View style={styles.left_side}>
        <Pressable style={styles.left_side_avatar} onPress={auth.signout} />
      </View>
      <View style={styles.right_side}>
        <View>
          <Text style={styles.right_side_number}>{post_count}</Text>
          <Text style={styles.right_side_title}>Posts</Text>
        </View>
        <View>
          <Text style={styles.right_side_number}>{post_count}</Text>
          <Text style={styles.right_side_title}>Posts</Text>
        </View>
        <View>
          <Text style={styles.right_side_number}>{post_count}</Text>
          <Text style={styles.right_side_title}>Posts</Text>
        </View>
      </View>
    </View>
  );
};

export default ProfileHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  left_side: {},
  left_side_avatar: {
    width: 80,
    height: 80,
    borderRadius: 80,
    backgroundColor: 'gray',
  },
  right_side: {
    marginLeft: 10,
    flexGrow: 1,
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
  },
  right_side_number: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  right_side_title: {
    textAlign: 'center',
    fontSize: 20,
  },
});
