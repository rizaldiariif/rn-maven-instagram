import React from 'react';
import {StyleSheet, Pressable, ImageBackground} from 'react-native';
import {useNavigation} from '@react-navigation/native';

interface Props {
  image_url: string;
  post_id: number;
}

const ProfilePost = (props: Props) => {
  const {post_id, image_url} = props;

  const navigation = useNavigation();

  return (
    <Pressable
      style={styles.item}
      onPress={() => navigation.navigate('PostDetail', {id: post_id})}>
      <ImageBackground
        source={{uri: `https://strapi-dev.maven.co.id${image_url}`}}
        style={styles.background_image}
      />
    </Pressable>
  );
};

export default ProfilePost;

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'gray',
    height: 125,
    flexGrow: 1,
    margin: 1,
  },
  background_image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
