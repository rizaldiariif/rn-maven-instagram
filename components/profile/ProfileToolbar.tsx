import React from 'react';
import {StyleSheet, Text, View, Pressable} from 'react-native';

const ProfileToolbar = () => {
  return (
    <View style={styles.container}>
      <Pressable style={styles.button}>
        <Text style={styles.button_text}>Edit Profile</Text>
      </Pressable>
      <Pressable style={styles.button}>
        <Text style={styles.button_text}>Saved</Text>
      </Pressable>
    </View>
  );
};

export default ProfileToolbar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  button: {
    flexGrow: 1,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderColor: 'gray',
    borderRadius: 5,
    margin: 10,
  },
  button_text: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
