import React from 'react';
import {StyleSheet, Pressable, Text, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import posts from '../../dev/post_data';

const ProfileHighlight = () => {
  return (
    <FlatList
      data={posts}
      keyExtractor={(item) => item.id}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      renderItem={({item}) => (
        <View>
          <Pressable style={styles.highlight} />
          <Text style={styles.highlight_title}>Title</Text>
        </View>
      )}
      style={styles.container}
    />
  );
};

export default ProfileHighlight;

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  highlight: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: 'gray',
    marginHorizontal: 15,
  },
  highlight_title: {
    textAlign: 'center',
  },
});
