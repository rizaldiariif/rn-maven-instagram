import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

interface Props {
  username: string;
}

const ProfileDescription = (props: Props) => {
  const {username} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.username}>{username}</Text>
      <Text>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi,
        fugit.
      </Text>
    </View>
  );
};

export default ProfileDescription;

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  username: {
    fontWeight: 'bold',
  },
});
