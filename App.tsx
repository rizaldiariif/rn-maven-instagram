import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {AuthProvider, useAuth} from './libs/auth';

import {
  ExploreStackParamList,
  HomeStackParamList,
  ProfileStackParamList,
  ShopStackParamList,
} from './types/navigation';

import HomeScreen from './screens/HomeScreen';
import PostDetailScreen from './screens/PostDetailScreen';
import ExploreScreen from './screens/ExploreScreen';
import ShopScreen from './screens/ShopScreen';
import ProfileScreen from './screens/ProfileScreen';
import {Text} from 'react-native';
import LoginScreen from './screens/LoginScreen';
import SplashScreen from './screens/SplashScreen';

const HomeStack = createStackNavigator<HomeStackParamList>();
const ExploreStack = createStackNavigator<ExploreStackParamList>();
const ShopStack = createStackNavigator<ShopStackParamList>();
const ProfileStack = createStackNavigator<ProfileStackParamList>();
const Tab = createBottomTabNavigator();

const HomeStackComponent = () => (
  <HomeStack.Navigator initialRouteName="Home">
    <HomeStack.Screen name="Home" component={HomeScreen} />
    <HomeStack.Screen
      name="PostDetail"
      component={PostDetailScreen}
      options={{title: 'Post'}}
    />
    <HomeStack.Screen name="Profile" component={ProfileScreen} />
  </HomeStack.Navigator>
);

const ExploreStackComponent = () => (
  <ExploreStack.Navigator initialRouteName="Explore">
    <ExploreStack.Screen name="Explore" component={ExploreScreen} />
    <ExploreStack.Screen
      name="PostDetail"
      component={PostDetailScreen}
      options={{title: 'Post'}}
    />
    <ExploreStack.Screen name="Profile" component={ProfileScreen} />
  </ExploreStack.Navigator>
);

const ShopStackComponent = () => (
  <ShopStack.Navigator initialRouteName="Shop">
    <ShopStack.Screen name="Shop" component={ShopScreen} />
  </ShopStack.Navigator>
);

const ProfileStackComponent = () => (
  <ProfileStack.Navigator initialRouteName="Profile">
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      initialParams={{id: 1}}
    />
    <ProfileStack.Screen name="PostDetail" component={PostDetailScreen} />
  </ProfileStack.Navigator>
);

const RenderArea = () => {
  const auth = useAuth();

  if (auth.loading || auth.user === null) {
    return <SplashScreen />;
  }

  if (auth.user === false) {
    return <LoginScreen />;
  }

  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeStackComponent} />
      <Tab.Screen name="Explore" component={ExploreStackComponent} />
      <Tab.Screen name="Shop" component={ShopStackComponent} />
      <Tab.Screen
        name="Profile"
        component={ProfileStackComponent}
        initialParams={{id: auth.user.id}}
      />
    </Tab.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <AuthProvider>
        <RenderArea />
      </AuthProvider>
    </NavigationContainer>
  );
};

export default App;
