interface Post {
  id: string;
  username: string;
}

const posts: Post[] = [
  {
    id: '1',
    username: 'user_1',
  },
  {
    id: '2',
    username: 'user_2',
  },
  {
    id: '3',
    username: 'user_3',
  },
  {
    id: '4',
    username: 'user_4',
  },
  {
    id: '5',
    username: 'user_5',
  },
  {
    id: '6',
    username: 'user_6',
  },
  {
    id: '7',
    username: 'user_7',
  },
  {
    id: '8',
    username: 'user_8',
  },
  {
    id: '9',
    username: 'user_9',
  },
  {
    id: '10',
    username: 'user_10',
  },
  {
    id: '11',
    username: 'user_11',
  },
  {
    id: '12',
    username: 'user_12',
  },
];

export default posts;
